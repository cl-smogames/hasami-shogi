;;;; cl-hasami-shogi v0.3
;;;;
;;;; Copyright (c) 2004,2005 Sami Makinen.  

;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:

;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;
;;; Try following:
;;;
;;;   (load "hasami-shogi" :if-source-newer :compile)
;;;   (hasami-shogi:start)
;;;

(defpackage #:hasami-shogi
  (:use :common-lisp)
  (:export #:start))

(in-package :hasami-shogi)

(declaim (optimize (speed 3) (safety 0) (debug 0) (space 0)))
;;(declaim (optimize (speed 0) (safety 3) (debug 3) (space 0)))

;;; Globals
(defparameter *cutoff-depth* 5)

;;;
;;; Structures and types
;;;

(deftype uint32 () '(unsigned-byte 32))
(deftype uint8  () '(unsigned-byte 8))
(deftype move   () '(simple-array (unsigned-byte 8) (4)))
(deftype weight () '(simple-array (single-float -1f0 1f0)))
(deftype limited-float () '(single-float -10000.0 10000.0))

(declaim (type uint32 *cutoff-depth*))

(defstruct player
  (human t   :type t)
  (agent nil :type (or function null))
  (color nil :type (or symbol null)))

(defstruct (weighted-agent (:include player))
  (work-board nil :type (or board null))
  (weights    nil :type (or (simple-array weight) null)))

(defstruct (move (:type (vector uint8)))
  (from-x 0 :type uint8)
  (from-y 0 :type uint8)
  (to-x   0 :type uint8)
  (to-y   0 :type uint8))
	   
(defstruct piece
  (x       0 :type uint8)
  (y       0 :type uint8)
  (color nil :type (or symbol null)))

(defstruct (board (:copier nil)) ; #'copy-board is provided explicitly.
  (width          0 :type uint8)
  (height         0 :type uint8)
  (white-pieces nil :type (or list null))
  (black-pieces nil :type (or list null))
  (squares      nil :type (or (array character) null)))

(defstruct (weight (:type (vector (single-float -1f0 1f0))))
  (empty    0.0 :type (single-float -1f0 1f0))
  (own      0.0 :type (single-float -1f0 1f0))
  (opponent 0.0 :type (single-float -1f0 1f0)))

(defstruct state
  (weights      nil :type (or (simple-array weight)  null))
  (weighted-sum 0.0 :type limited-float)
  (board        nil :type (or board  null))
  (work-board   nil :type (or board  null))
  (max-plr      nil :type (or player null))
  (min-plr      nil :type (or player null))
  (in-turn      nil :type (or player null))
  (idle-plr     nil :type (or player null))
  (moves        nil :type (or list   null))
  (move         nil :type (or move   null)))

;;;
;;; Macros
;;;
(defmacro swap-players (in-turn idle)
  "Swap current in-turn player and idle player, so that idle player
becomes in-turn player."
  (let ((tmp (gensym)))
    `(let ((,tmp ,in-turn))
      (setq ,in-turn ,idle
       ,idle    ,tmp))))

(defmacro message (fmt &rest args)
  (declare (type string fmt))
  "Shows a message to a player. Syntax is same as in format function."
  `(format t ,fmt ,@args))

(defmacro error-message (fmt &rest args)
  (declare (type string fmt))
  "Shows an error message to a player. Syntax is same as in format
function."
  `(format t ,fmt ,@args))

(defmacro get-weight (weights x y)
  (declare (type (simple-array weight) weights)
           (type uint8 x y))
  "Returns weight in board position (x, y)."
  `(aref ,weights (1- ,y) (1- ,x)))

(defmacro get-opponent-color (plr)
  (declare (type player plr))
  "Returns player's opponent's color."
  `(if (eq (player-color ,plr) 'white)
    'black
    'white))

(defmacro get-my-ch (plr)
  (declare (type player plr))
  "Returns a character representing player's piece on board."
  `(if (eq (player-color ,plr) 'white)
    #\O
    #\X))

(defmacro with-legal-moves ((move board plr) &body body)
  "A macro to iterate through player's legal moves. body is executed
for every legal move and current move in turn is bind to given move
variable."
  (let ((p (gensym "PIECE"))
	(x (gensym "X"))
	(y (gensym "Y")))
  `(let ((,move (make-move)))
    (dolist (,p (get-player-pieces ,board ,plr))
      (setf (move-from-x ,move) (piece-x ,p)
	    (move-from-y ,move) (piece-y ,p) 
	    (move-to-y   ,move) (piece-y ,p))
      (loop 
       for ,x of-type uint8 from 1 to (board-width ,board)
       do 
       (setf (move-to-x ,move) ,x)
       (when (is-legal-p ,board ,move ,plr)
	 ,@body))

      (setf (move-to-x ,move) (piece-x ,p))
      (loop 
       for ,y of-type uint8 from 1 to (board-height ,board)
       do 
       (setf (move-to-y ,move) ,y)
       (when (is-legal-p ,board ,move ,plr) 
	 ,@body))))))

(defmacro weighted-sum-delta (weights move removed &optional opponent)
  (declare (type simple-array weights)
	   (type move move)
	   (type list removed)
	   (type limited-float))
  "Returns the difference of weighted sum when move is executed. If opponent is t,
delta is calculated on behalf of opponent player."
  (let ((wremoved 'weight-opponent)
        (wmoved   'weight-own))
    (when opponent
      (setq wremoved 'weight-own
            wmoved   'weight-opponent))
    `(the limited-float
       (+
        (loop for p in ,removed
              sum 
              (- (weight-empty (get-weight ,weights (piece-x p) (piece-y p)))
                 (,wremoved    (get-weight ,weights (piece-x p) (piece-y p))))
              of-type limited-float)
        (- (+ (weight-empty (get-weight ,weights (move-from-x ,move) (move-from-y ,move)))
              (,wmoved      (get-weight ,weights (move-to-x ,move)   (move-to-y ,move))))
           (,wmoved      (get-weight ,weights (move-from-x ,move) (move-from-y ,move)))
           (weight-empty (get-weight ,weights (move-to-x ,move)   (move-to-y ,move))))))))

;;;
;;; Functions    
;;;
(defun copy-board (board)
  (declare (type board board))
  "Returns a copy of given board."
  (let ((copy (make-board :width (board-width board) :height (board-height board))))
    (setf (board-white-pieces copy) (map 'list #'copy-piece (board-white-pieces board))
          (board-black-pieces copy) (map 'list #'copy-piece (board-black-pieces board)))
    (setf (board-squares copy) (make-array (list (board-width board) (board-height board))
                                           :element-type 'character :initial-element #\Space))
    (dolist (wp (board-white-pieces copy))
      (set-square copy wp))
    (dolist (bp (board-black-pieces copy))
      (set-square copy bp))
    copy))

(defun update-work-board (state)
  (declare (type state state))
  "Destructively modifies state's work board by making it identical to board."
  (set-work-board (state-board state) (state-work-board state))
  (loop with in-turn = (state-max-plr state)
	with idle    = (state-min-plr state)
	for m in (state-moves state)
	do
	(exec-move (state-work-board state) in-turn m)
	(swap-players in-turn idle)))

(defun get-piece-legal-moves (board plr p)
  (declare (type board board)
           (type player plr)
           (type piece p))
  "Returns a list of legal moves of given piece on current board situation.
Each list element is a list of four numbers: from-x, from-y, to-x, to-y."
  (nconc
   (loop with move = nil
	 for x of-type uint8 from 1 to (board-width board)
	 do (setq move (make-move :from-x (piece-x p) :from-y (piece-y p) :to-x x :to-y (piece-y p)))
	 when (is-legal-p board move plr) collect move)
   (loop with move = nil
	 for y of-type uint8 from 1 to (board-height board)
	 do (setq move (make-move :from-x (piece-x p) :from-y (piece-y p) :to-x (piece-x p) :to-y y))
	 when (is-legal-p board move plr) collect move)))

(defun get-player-legal-moves (board plr)
  (declare (type board board)
           (type player plr))
  "Returns list of legal player moves in current board position."
  (loop for p in (get-player-pieces board plr)
        nconc (get-piece-legal-moves board plr p)))

(defun set-work-board (board work-board)
  (declare (type board board work-board))
  "Destructively updates work-board by making it identical to board."
  (setf (board-white-pieces work-board) (map 'list #'copy-piece (board-white-pieces board))
        (board-black-pieces work-board) (map 'list #'copy-piece (board-black-pieces board)))
  (loop for y of-type uint8 from 0 below (board-height board)
	do 
	(loop for x of-type uint8 from 0 below (board-width board)
	      do
	      (setf (aref (board-squares work-board) y x)
		    (aref (board-squares board) y x)))))

(defun create-weighted-agent (color board)
  "Returns instance of weighted agent computer opponent. "
  (make-weighted-agent :human       nil
                       :agent     #'best-move-by-weights
                       :color       color
                       :weights    (init-random-weights (board-width board) (board-height board))
                       :work-board (init-game :w (board-width board) :h (board-height board))))

(defun create-alpha-beta-agent (color board)
  "Returns instance of alpha-beta agent computer opponent."
  (make-weighted-agent :human       nil
                       :agent     #'alpha-beta-search
                       :color       color
                       :weights    (init-fixed-weights (board-width board) (board-height board))
                       :work-board (init-game :w (board-width board) :h (board-height board))))

(defun init-fixed-weights (w h)
  (declare (type uint8 w h))
  "Returns array of weights for board of size width w and height h."
  (loop with weights = (make-array (list w h) :element-type '(or weight null) :initial-element nil)
	for y of-type uint8 from 0 below h
	finally (return weights)
	do
	(loop for x of-type uint8 from 0 below w
	      do
	      (setf (aref weights y x) (make-weight :empty     0.0
						    :own       0.2
						    :opponent -0.2)))))

(defun init-random-weights (w h)
  (declare (type uint8 w h))
  "Returns array of weights for board of size width w and height h."
  (flet ((random-single-float (max)
	   (declare (type single-float max)
		    (type single-float))
	   (the single-float (random max))))
    (loop with weights = (make-array (list w h) :element-type '(or weight null) :initial-element nil)
	  for y of-type uint8 from 0 below h
	  finally (return weights)
	  do
	  (loop for x of-type uint8 from 0 below w
		do
		(setf (aref weights y x) (make-weight :empty    (random-single-float 1f0) 
						      :own      (random-single-float 1f0) 
						      :opponent (random-single-float 1f0)))))))

(defun calculate-weighted-sum (plr board weights)
  (declare (type limited-float)
	   (type player plr)
	   (type board board)
	   (type (simple-array weight) weights))
  "Returns weighted sum of board positions (utility) from given
player's point of view. Sum is calculated with given weights."
  (loop with sum of-type limited-float = 0f0
	with ch of-type standard-char = #\Space
	with weight = nil
	finally (return sum)
	for y of-type uint8 from 1 to (board-height board) do
	(loop for x of-type uint8 from 1 to (board-width board) do
	      (setq ch     (get-square board   x y)
		    weight (get-weight weights x y))
	      (incf sum 
		    (if (eql ch #\Space)
			(weight-empty weight)
			(if (eql ch (get-my-ch plr))
			    (weight-own weight)
			    (weight-opponent weight)))))))               

(defun alpha-beta-search (board me opponent)
  "Returns a move determined by alpha-beta pruning algorithm."
  (let ((best-move  nil)
	(best-depth 1)
        (best-alpha -10000.0)
        (delta-t    0)
        (start-time 0)
	(state     (make-initial-state board (weighted-agent-weights me) me opponent))
	(number-of-moves 0))
    (declare (type uint32 start-time best-depth number-of-moves)
	     (type limited-float best-alpha))
    (setq start-time (get-universal-time))

    ;; initialize work board identical to original and calculate
    ;; starting weighted sum for the board position. Board position
    ;; and weighted sum is updated after move and then rolled back
    ;; to original after the move is evaluated.
    (update-work-board state)
    (setf (state-weighted-sum state) (calculate-weighted-sum me board (state-weights state)))
    
    (labels ((evaluate-utility (state)
	       (declare (type limited-float))
	       "Returns evaluated utility from current state calculated for the max player."
	       (let ((winner (determine-winner (state-board state))))
		 (if winner
		     (if (eq winner (player-color (state-max-plr state)))
			 1000.0
			 -1000.0)
		     (state-weighted-sum state))))
	     
	     (cutoff-p (state depth)
	       (declare (type state  state)
			(type uint32 depth))
	       "Predicate to test if search should stop."
	       (or (game-over-p (state-board state))
		   (>= depth *cutoff-depth*)))
	     
	     (max-value (depth alpha beta)
	       (declare (type uint32 depth)
                        (type limited-float alpha beta)
			(type limited-float))
	       "Returns maximized utility for given state."
	       (incf number-of-moves)
	       (if (cutoff-p state depth)
		   (evaluate-utility state)
		   (let ((plr     (state-max-plr state))
			 (wb      (state-work-board state))
			 (removed  nil)
			 (wsum     0.0))
		     (declare (type limited-float wsum))
		     (block max-prune
		       (with-legal-moves (move wb plr)
			 (setq wsum    (state-weighted-sum state)
			       removed (exec-move wb plr move))
			 (setf (state-weighted-sum state) 
			       (+ (state-weighted-sum state) 
				  (weighted-sum-delta (state-weights state) move removed)))
			 (setq alpha (max alpha (the limited-float (min-value (the uint32 (1+ depth)) alpha beta))))
			 (rollback-move wb plr move removed)
			 (setf (state-weighted-sum state) wsum)
			 (when (and (eql depth 0)
                                    (or (not best-move)
                                        (> alpha best-alpha) 
                                        (and (equalp alpha best-alpha) 
                                             (> best-depth 0))))
			   (format t "Best move: ~A~%" move)
			   (setq best-move (copy-move move)
				 best-depth depth
				 best-alpha alpha))
			 (when (> depth 1)
			   (when (<= beta alpha)
			     (return-from max-prune beta))))
		       (return-from max-prune alpha)))))
	     
	     (min-value (depth alpha beta)
	       (declare (type uint32 depth)
                        (type limited-float alpha beta)
			(type limited-float))
	       "Returns minimized utility for given state."
	       (incf number-of-moves)
	       (if (cutoff-p state depth)
		   (evaluate-utility state)
		   (let ((plr     (state-min-plr state))
			 (wb      (state-work-board state))
			 (removed  nil)
			 (wsum     0.0))
		     (declare (type limited-float wsum))
		     (block min-prune
		       (with-legal-moves (move wb plr)
			 (setq wsum    (state-weighted-sum state)
			       removed (exec-move wb plr move))
			 (setf (state-weighted-sum state) 
			       (+ (state-weighted-sum state) 
				  (weighted-sum-delta (state-weights state) move removed t)))
			 (setq beta (min beta (the limited-float (max-value (the uint32 (1+ depth)) alpha beta))))
			 (rollback-move wb plr move removed)
			 (setf (state-weighted-sum state) wsum)
			 (when (and (> depth 1) (<= beta alpha))
			   (return-from min-prune alpha)))
		       (return-from min-prune beta))))))
      (max-value 0 -10000.0 10000.0))
    (setq delta-t (the (unsigned-byte 32) (- (get-universal-time) start-time)))
    (unless best-move
      (error "No move found!"))
    (unless  (is-legal-p board best-move me)
      (error "Move ~A is not legal!" best-move))
    (message "~D moves considered in ~D seconds.~%" number-of-moves delta-t)
    (message "In average ~D moves/s.~%" (round (/ number-of-moves (max delta-t 1))))
    best-move))

(defun make-initial-state (board w plr opponent)
  "Returns initial game state."
  (make-state :weights     w 
	      :board       board 
	      :work-board (weighted-agent-work-board plr)
	      :max-plr     plr 
	      :min-plr     opponent
	      :in-turn     plr
	      :idle-plr    opponent))

(defun best-move-by-weights (board plr)
  (declare (type board board)
           (type player plr))
  "Returns a move determined by weights."
  (let ((value 0.0)
        (max -10000.0)
        (best nil)
        (move (make-move)))
    (declare (type limited-float value max))
    (dolist (p (get-player-pieces board plr))
      (setf (move-from-x move) (piece-x p)
            (move-from-y move) (piece-y p)
	    (move-to-y move)   (piece-y p))
      (loop for x of-type uint8 from 1 to (board-width board) do
            (setf (move-to-x move) x)
            (when (is-legal-p board move plr)
              (set-work-board (weighted-agent-work-board plr) board)
              (exec-move (weighted-agent-work-board plr) plr move)
              (setq value (calculate-weighted-sum plr
                                                  (weighted-agent-work-board plr)
                                                  (weighted-agent-weights plr)))
              (when (> value max)
                (setq max  value
                      best (copy-move move)))))
      (setf (move-to-x move) (piece-x p))
      (loop for y of-type uint8 from 1 to (board-height board) do
	    (setf (move-to-y move) y)
            (when (is-legal-p board move plr)
              (set-work-board (weighted-agent-work-board plr) board)
              (exec-move (weighted-agent-work-board plr) plr move)
              (setq value (calculate-weighted-sum plr
                                                  (weighted-agent-work-board plr)
                                                  (weighted-agent-weights plr)))
              (when (> value max)
                (setq max  value
                      best (copy-move move))))))
    (unless (and best (find-piece board (move-from-x best) (move-from-y best) (player-color plr)))
      (error "Move not found."))
    best))
                    
(defun get-player-pieces (board plr)
  (declare (type board board)
           (type player plr))
  "Returns list of given player's pieces on board."
  (if (eq (player-color plr) 'white)
      (board-white-pieces board)
      (board-black-pieces board)))
      
(defun random-agent (board me opponent)
  (declare (type board board)
           (type player me)
	   (ignore opponent))
  "A simple computer opponent which does moves randomly.
Returns a move as list of four numbers (from-x from-y to-x to-y)."
    (loop with my-pieces           = (get-player-pieces board me)
	  with piece               = nil
	  with move                = nil
	  with to-x of-type uint8 = 0
	  with to-y of-type uint8 = 0
	  until (and move (is-legal-p board move me)) 
	  finally (return move)
	  do
          (setq piece (nth (random (length my-pieces)) my-pieces))
          (setq to-x (piece-x piece)
                to-y (piece-y piece))
          (if (< (random 1f0) 0.5)
              (setq to-x (1+ (random (board-width board))))
	      (setq to-y (1+ (random (board-height board)))))
          (setq move (make-move :from-x (piece-x piece)
				:from-y (piece-y piece)
				:to-x to-x 
				:to-y to-y))))

(defun print-squares (board)
  (declare (type board board))
  "Print game board and pieces on it with simple ascii graphics."
  (message "  ")
  (dotimes (x (board-width board))
    (message " ~C " (code-char (+ (char-int #\A) x))))
  (message "~%")
  (loop for y of-type uint8 from (board-height board) downto 1 do
        (message "  ")
        (dotimes (x (board-width board))
          (message "+--"))
        (message "+")
        (when (eql y 1)
          (message "    X black (~D pieces)" (length (board-black-pieces board))))
        (when (eql y 2)
          (message "    O white (~D pieces)" (length (board-white-pieces board))))
        (message "~% ~D" (mod y 10))
        (loop for x from 1 to (board-width board) do
              (message "|~C " (get-square board x y)))
        (message "|~%"))
  (message "  ")
  (dotimes (x (board-width board))
    (message "+--"))
  (message "+~%"))
  
(defun clear-square (board piece)
  (declare (type board board)
           (type piece piece))
  "Remove piece from square leaving the square empty."
  (setf (aref (board-squares board) (1- (piece-y piece)) (1- (piece-x piece))) #\Space))

(defun set-square (board piece)
  (declare (type board board)
           (type piece piece))
  "Set piece to square."
  (let (ch)
    (if (eq (piece-color piece) 'white)
        (setq ch #\O)
	(setq ch #\X))
    (setf (aref (board-squares board) (1- (piece-y piece)) (1- (piece-x piece))) ch)))

(defun get-square (board x y)
  (declare (type board board)
           (type uint8 x y))
  "Return character in square (x, y). Character is either #\X,
if square has black piece on it, #\O if square has white piece
on it or #\Space, if square has no piece."
  (when (and  (> x 0)
              (> y 0)
              (<= x (board-width board))
              (<= y (board-height board)))
    (aref (board-squares board) (1- y) (1- x))))

(defun move-piece (board piece to-x to-y)
  (declare (type board board)
           (type piece piece)
           (type uint8 to-x to-y))
  "Destructively update board by moving given piece to given (x, y) position.
The legality of move is not checked."
  (clear-square board piece)
  (setf (piece-x piece) to-x
        (piece-y piece) to-y)
  (set-square board piece))

(defun find-piece (board x y color)
  (declare (type board  board)
           (type uint8 x y)
           (type symbol color))
  "Return board's piece from given (x, y) position and color. If no piece with
given color in given position is found, nil is returned."
  (let (l)
    (if (eq color 'white)
        (setq l (board-white-pieces board))
	(setq l (board-black-pieces board)))
    (find-if #'(lambda (p)
                 (and (eql (piece-x p) x)
                      (eql (piece-y p) y))) l)))

(defun add-piece (board piece)
  "Destructively modifies one of board's piece list and square array
by adding piece to game."
  (set-square board piece)
  (if (eq (piece-color piece) 'white)
      (setf (board-white-pieces board) (append (list piece) (board-white-pieces board)))
      (setf (board-black-pieces board) (append (list piece) (board-black-pieces board)))))

(defun remove-piece (board piece)
  (declare (type board board)
           (type piece piece))
  "Destructively modifies one of board's piece list and square array
by removing piece from game."
  (clear-square board piece)
  (if (eq (piece-color piece) 'white)
      (setf (board-white-pieces board) (remove piece (board-white-pieces board)))
      (setf (board-black-pieces board) (remove piece (board-black-pieces board)))))

(defun create-initial-pieces (board color)
  (declare (type board board)
           (type symbol color))
  "Returns a list of initial pieces in their starting positions of given color."
  (let ((y 1))
    (when (eq color 'white)
      (setq y (board-height board)))
    (loop for i from 1 to (board-width board)
          collect (make-piece :x i :y y :color color))))

(defun show-rules ()
  (message "The object is to trap all but one of the opponent's
pieces. The pieces move exactly like the rook in Chess or Shogi -
straight forwards, backwards or sideways over any number of vacant
squares.

Capture is by the escort method: the victim is trapped, either
vertically or horizontally between two pieces of the other
player. More than one piece can be captured on a move, if those pieces
are linked together. A piece can move safely to situation were
it would be normally captured, i.e. player cannot cause capture
of his/hers own pieces.

Captured pieces are removed from the play.~%"))

(defun show-help ()
  (message "
Usage:

:h      show this text,
:q      quit game,
:r      show game rules,
:s      show board,
:a      about the game

x y n   move piece from position (x,y) to
       (x,n), if n is a number or to
       (n,y), if n is a character.~%"))

(defun show-intro ()
  (message "

           HASAMI-SHOGI v0.3

  Copyright (c) 2004,2005 Sami Makinen.
"))

(defun quit-game ()
  (throw 'quit "Quit."))

(defun parse-command (board l)
  (declare (type board board)
           (type list l))
  "Parse user given input which starts with colon character (#\:).
Function interprets given input as a special command and calls
corresponding handler."
  (let (ch)
    (pop l)
    (setq ch (char-upcase (car l)))
    (case ch
      (#\H (show-help))
      (#\Q (quit-game))
      (#\R (show-rules))
      (#\S (print-squares board))
      (#\A (show-intro))
      (t (error-message "Unknown command.~%")))
    nil))

(defun parse-move (l)
  (declare (type list l))
  "Parse user given input assuming the input is player's game move.
In case of error, error message is shown and function returns nil.  If
parse is successful, a list of four numbers is returned:
from-x, from-y, to-x and to-y."
  (let (x y to-x to-y)
    (block safe-parse
      (setq l (remove #\Space l))
      (unless (and l
                   (first  l) (characterp (first  l))
                   (second l) (characterp (second l))
                   (third  l) (characterp (third  l)))
        (error-message "Illegal input (type :h for help)~%")
        (return-from safe-parse))
      (setq x (- (char-code (char-upcase (first l))) (char-code #\A) -1))
      (setq y (- (char-code (second l)) (char-code #\1) -1))
      (if (and (<= (char-code #\1) (char-code (third l)))
               (>= (char-code #\9) (char-code (third l)))) ; input is a number
          (setq to-x x 
                to-y (- (char-code (third l)) (char-code #\1) -1))
        (setq to-y y
              to-x (- (char-code (char-upcase (third l))) (char-code #\A) -1)))
      (unless (and (> x 0) (> y 0) (> to-x 0) (> to-y 0))
        (error-message "Illegal input (type :h for help)~%")
        (return-from safe-parse))
      (make-move :from-x x :from-y y :to-x to-x :to-y to-y))))

(defun parse-input (board str)
  (declare (type board board)
           (type string str))
  "Parse user's input."
  (loop with l of-type list = (coerce str 'list)
	while (member (car l) '(#\Space #\Tab))
	do (pop l)
	finally 
	(return
	  (if (eql (car l) #\:)
	      (parse-command board l)
	      (parse-move l)))))

(defun is-legal-p (board move plr &optional verbose)
  (declare (type board  board)
           (type move   move)
           (type player plr))
  "Predicate to check if given move is legal."
  (let ((x0 (move-from-x move))
        (y0 (move-from-y move))
        (x1 (move-to-x   move))
        (y1 (move-to-y   move))
        (start 0)
        (end 0)
        (piece nil))
    (declare (type uint8 x0 y0 x1 y1 start end))
    (block legality-check
      (setq piece (find-piece board x0 y0 (player-color plr)))
      (unless piece  ; check if player has a piece in given coordinate
        (when verbose 
	  (error-message "No piece in given coordinate (~D, ~D)~%" x0 y0))
        (return-from legality-check))
      (when (or (< x1 1)	 ; check 'to' coordinates are in board
                (< y1 1)
                (> x1 (board-width board))
                (> y1 (board-height board)))
        (when verbose 
	  (error-message "Destination coordinate (~D, ~D) not inside board.~%" x1 y1))
        (return-from legality-check))
      (when (and (eql x0 x1)	 ; check 'from' and 'to' are not same.
                 (eql y0 y1))
        (when verbose 
	  (error-message "Destination coordinate (~D, ~D) is same as original (~D, ~D).~%" x1 y1 x0 y0))
        (return-from legality-check))
      ;; check move is either horizontal or vertical
      (unless (eql x0 x1)		; horizontal move
        (unless (eql y0 y1)
          (when verbose 
	    (error-message "Move has to be either vertical or horizontal!~%" ))
          (return-from legality-check))
        (if (< x0 x1)
            (setq start (1+ x0)
                  end x1)
	    (setq start x1
		  end (1- x0)))
        (loop for x of-type uint8 from start to end do ; squares piece moves over need to be empty
              (unless (eql (get-square board x y0) #\Space)
                (when verbose 
		  (error-message "Piece cannot move over other pieces.~%"))
                (return-from legality-check))))
      (unless (eql y0 y1)		; vertical move
        (unless (eql x0 x1)
          (when verbose 
	    (error-message "Move has to be either vertical or horizontal!~%" ))
          (return-from legality-check))
        (if (< y0 y1)
            (setq start (1+ y0)
                  end y1)
	    (setq start y1
		  end (1- y0)))
        (loop for y of-type uint8 from start to end do
              (unless (eql (get-square board x0 y) #\Space)
                (when verbose 
		  (error-message "Piece cannot move over other pieces.~%"))
                (return-from legality-check))))
      t)))
      

(defun read-move (board plr)
  (declare (type board board)
           (type player plr))
  "Read human player's move from terminal."
  (message "(type :h for help, :r for rules, :a for about, :q to quit).~%")
  (loop with move  = nil
	with input = nil
	finally (return move)
	until move do
	(message "Your move: ")
	(setq input (read-line)
	      move (parse-input board input))
	(when (and move (not (is-legal-p board move plr t)))
	  (error-message "Illegal move!~%")
	  (setq move nil))))

(defun remove-captured (board moved-piece)
  (declare (type board board)
           (type piece moved-piece))
  "Desctructively modifies board by removing all captured pieces from
board the moved-piece captures. Returns list of removed pieces."
  (macrolet ((collect-candidates (board piece axis dir color plr-ch opp-ch)
	       (let (max cur-y cur-x start)
		 (when (eq axis 'x) 
		   (setq cur-y (list 'piece-y piece)
			 cur-x 'pos)
		   (if (eq dir 'to)
		       (setq start (list '1+ (list 'piece-x piece))
			     max (list 'board-width board))
		       (setq start (list '1- (list 'piece-x piece))
			     max 1)))
		 (when (eq axis 'y) 
		   (setq cur-y 'pos
			 cur-x (list 'piece-x piece))
		   (if (eq dir 'to)
		       (setq start (list '1+ (list 'piece-y piece))
			      max (list 'board-height board))
		       (setq start (list '1- (list 'piece-y piece))
			     max 1)))
		 `(loop 
		   for     pos from ,start ,dir ,max
		   while   (eql (get-square ,board ,cur-x ,cur-y) ,opp-ch)
		   collect (find-piece ,board ,cur-x ,cur-y ,color) into candidates
		   finally (return 
			     (when (eql (get-square ,board ,cur-x ,cur-y) ,plr-ch)
			       candidates))))))
  (let (my-ch
        opp-ch
        opp-color
        (to-be-removed nil))
    (if (eq (piece-color moved-piece) 'white)
        (setq my-ch #\O
              opp-ch #\X
              opp-color 'black)
        (setq my-ch #\X
              opp-ch #\O
              opp-color 'white))
    (setq to-be-removed (nconc
			 (collect-candidates board moved-piece x to opp-color my-ch opp-ch)
			 (collect-candidates board moved-piece x downto opp-color my-ch opp-ch)
			 (collect-candidates board moved-piece y to opp-color my-ch opp-ch)
			 (collect-candidates board moved-piece y downto opp-color my-ch opp-ch)))
    (dolist (p to-be-removed)
      (remove-piece board p))
    to-be-removed)))

(defun rollback-move (board plr move removed-pieces)
  (declare (type board  board)
	   (type player plr)
	   (type move   move)
	   (type list   removed-pieces))
  "Rollback player's move. Removed pieces by move are put back into play."
  (let ((piece (find-piece board (move-to-x move) (move-to-y move) (player-color plr))))
;;    (format t "piece: ~A~%" piece)
    (move-piece board piece (move-from-x move) (move-from-y move))
    (dolist (p removed-pieces)
      (add-piece board p))))
      
(defun exec-move (board plr move)
  (declare (type board  board)
           (type player plr)
           (type move   move))
  "Execute player's move. Piece position is updated and captured
pieces are removed. Returns list of removed pieces."
  (let ((piece (find-piece board (move-from-x move) (move-from-y move) (player-color plr))))
    (move-piece board piece (move-to-x move) (move-to-y move))
    (remove-captured board piece)))

(defun game-over-p (board)
  (declare (type board board))
  "Check if game has ended."
  (or (< (length (board-white-pieces board)) 2)
      (< (length (board-black-pieces board)) 2)))

(defun determine-winner (board)
  (declare (type board board))
  "Returns color of game winner (black or white). If no winner is
found, nil is returned."
  (if (< (length (board-white-pieces board)) 2)
      'black
      (if (< (length (board-black-pieces board)) 2)
	  'white
	  nil)))
    
(defun game-loop (board plr1 plr2)
  (declare (type board board)
           (type player plr1 plr2))
  "Enters to main game loop. Function does not return until game ends
or player quits. "
  (let ((in-turn plr1)
        (idle    plr2)
        (move    nil))
    (loop until (game-over-p board) do
          (print-squares board)
          (message "~A in turn.~%" (player-color in-turn))
          (sleep 1)
          (if (player-human in-turn)
              (setq move (read-move board in-turn))
	      (setq move (funcall (player-agent in-turn) board in-turn idle)))
          (exec-move board in-turn move)
          (swap-players in-turn idle))
    (print-squares board)
    (case (determine-winner board)
      ('black (message "Black wins.~%"))
      ('white (message "White wins.~%"))
      (t      (message "Game is tie.")))))
          
(defun init-game (&key (w 9) (h 9))
  "Returns a game board of given width and height in its starting position."
  (let ((board (make-board :width w :height h)))
    (setf (board-white-pieces board) (create-initial-pieces board 'white))
    (setf (board-black-pieces board) (create-initial-pieces board 'black))
    (setf (board-squares board) (make-array (list w h) :element-type 'character :initial-element #\Space))
    (dolist (wp (board-white-pieces board))
      (set-square board wp))
    (dolist (bp (board-black-pieces board))
      (set-square board bp))
    board))

(defun query-player-types ()
  (loop with line of-type simple-base-string = ""
	with ch of-type standard-char = #\Space
	with choice of-type uint8 = 0
	while (or (< choice 1) (> choice 4)) 
	finally (return choice)
	do
	(message "

Select Game Type (black makes first move):

  [1]  Human    vs. Human      [3]  Human    (black) vs. Computer (white),
  [2]  Computer vs. Computer   [4]  Computer (black) vs. Human (white),
  [q]  Quit

Your choice [1-4q]: ")
	(setq line (read-line))
	(unless (string= line "")
	  (setq ch (char line 0))
	  (when (characterp ch)
	    (when (eql (char-upcase ch) #\Q)
	      (throw 'quit "Quit."))
	    (setq choice (- (char-code ch) (char-code #\0)))))))

(defun start ()
  "Start Hasami-Shogi game."
  (let ((board (init-game))
        (plr1 nil)
        (plr2 nil))
    (show-intro)
    (catch 'quit
      (case (query-player-types)
        (1 (setq plr1 (make-player :human t   :agent nil            :color 'black)
                 plr2 (make-player :human t   :agent nil            :color 'white)))
        (2 (setq plr1 (make-player :human nil :agent #'random-agent :color 'black)
                 plr2 (create-alpha-beta-agent 'white board)))
        (3 (setq plr1 (make-player :human t   :agent nil            :color 'black)
                 plr2 (create-alpha-beta-agent 'white board)))
        (4 (setq plr1 (make-player :human nil :agent #'random-agent :color 'black)
                 plr2 (create-alpha-beta-agent 'white board))))
      (game-loop board plr1 plr2))))
